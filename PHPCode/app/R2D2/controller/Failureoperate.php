<?php
namespace  app\R2D2\controller;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;
use think\mongo;
use  think\Request;


class  Failureoperate {


    function resultPrevious() {

        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];

        $ko = input('get.ko');
        $pn = input('get.pn');
        $sn = input('get.sn');

        $db = new mongo\Connection($dataBaseConfig);

        if($ko==""){

            $output = array('info' => 'error', 'code' => -201, 'data' => 'please input KO.');
            exit(json_encode($output));
        }elseif (empty($pn)) {

            $output = array('info' => 'error', 'code' => -201, 'data' => 'please input PN.');
            exit(json_encode($output));
        }elseif (!empty($sn)){

            $rs = $db->name('troubleshooting.failureinfo')
                ->where('failDesc.pn', $pn)
                ->where('failDesc.sn', $sn)
                ->where('ko', (int)$ko)
                ->order('failID','desc')
                ->limit(1)
                ->select();

            if(empty($rs)){

                $output = array('info' => 'correct', 'code' => 200, 'data' => 'no record');

            }else {
                $success = $rs[0]['success'];
                $actionCount = $rs[0]['actionId'];

//failed
                if((int)$success == 0 && empty($actionCount)) {

                    $output = array('info' => 'correct',
                                    'code' => 200,
                                    'data' => 'failed',
                                    'detail'=> $rs[0]);
//in repair
                }elseif ((int)$success == 0 && !empty($actionCount)){

                    $output = array('info' => 'correct',
                        'code' => 200,
                        'data' => 'in repair',
                        'detail'=> $rs[0]);

//fixed
                }elseif((int)$success == 1) {

                    $output = array('info' => 'correct',
                                     'code' => 200,
                                     'data' => 'fixed',
                                    'detail'=> $rs[0]);
                }
            }
            print(json_encode($output));
        }

    }


    function getSuggestion() {

        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];

        $ko = input('get.ko');
        $pn = input('get.pn');
        $sn = input('get.sn');
        $tc = input('get.tc');

        $db = new mongo\Connection($dataBaseConfig);

        if($ko!=""&&empty($pn)&&empty($sn)){

            $rs = $db->name('troubleshooting.actionform')
                ->where('ko', (int)$ko)
                ->select();
            $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
            exit(json_encode($output));

        }

        if(($ko==""||$pn==""||$sn=="")&&($ko==""||$pn==""||$tc=="")){
            $output = array('info' => 'error', 'code' => -200, 'data' => "Input error");
            exit(json_encode($output));
        }

        $rs = $db->name('troubleshooting.actionform')
            ->where('ko', (int)$ko)
            ->select();
        $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
        exit(json_encode($output));


//        if($ko==""){
//
//            $output = array('info' => 'error', 'code' => -201, 'data' => 'please input KO.');
//            exit(json_encode($output));
//        }elseif (empty($pn)){
//
//            $output = array('info' => 'error', 'code' => -201, 'data' => 'please input PN.');
//            exit(json_encode($output));
//        }elseif (!empty($sn)&&empty($tc)){
//
//            $rs = $db->name('troubleshooting.failureinfo')
//                ->where('failDesc.pn', $pn)
//                ->where('failDesc.sn', $sn)
//                ->where('ko', (int)$ko)
//                ->field('actionId')
//                ->limit(1)
//                ->select();
//
//            $suggest = $rs[0]['actionId'];  //获取建议ID的数组
//
//            $data = $db->name('troubleshooting.actionform')
//                ->where('actionID','in', $suggest)
//                ->select();
//
//            $output = array('info' => 'correct', 'code' => 200, 'data' => $data);
//            exit(json_encode($output));
//
//        }elseif (empty($sn)&&!empty($tc)) {
//
//            $rs = $db->name('troubleshooting.failureinfo')
//                ->where('failDesc.pn', $pn)
//                ->where('failDesc.tc', $tc)
//                ->where('failDesc.ko', (int)$ko)
//                ->select();
//            $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
//            exit(json_encode($output));
//
//        }else {
//
//            $output = array('info' => 'error', 'code' => -201, 'data' => 'Please check the inputs.');
//            exit(json_encode($output));
//        }

    }


    function insert(){
        $timeStamp = input('post.timeStamp');
//         date('Y-m-d h:i:s', $timeStamp)
        $data = input('post.desc/a');
        $ko = input('post.ko');

        //输入是否为空
        if($ko==""||$data==""||$timeStamp==""){
            $output = array('info' => 'error', 'code' => -201, 'data' => "Input error");
            exit(json_encode($output));
        }
        //检测是否重复输入 框架where()无法匹配decode_json格式的输入，因此采用原生代码进行查询
        //也无法采用upsert的方式，因为failID无法获取
        //待改进(坑)
        $manager = new Manager("mongodb://localhost:27017");
        $filter  = ['failDesc' => $data , 'ko' => (int)$ko, 'timeStamp' =>(int)$timeStamp];
        $query   = new Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.failureinfo', $query)->toArray();
        if(count($rows) != 0) {
            $output = array('info' => 'error', 'code' => -201, 'data' => 'Repeat Input');
            exit(json_encode($output));
        }

        //不是重复数据后写入数据库
        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];
        $db = new mongo\Connection($dataBaseConfig);
        $autoId = getNextIdFromTable('failureInfo');

        $rs = $db->name('troubleshooting.failureinfo')->insert(["failID" => $autoId,"timeStamp" => (int)$timeStamp,
            'failDesc' => $data, "ko" => (int)$ko,'success' =>(int)0], "IGNORE");

        if($rs) {

            $output = array('info' => 'correct', 'code' => 200, 'data' => "success");
            print(json_encode($output));
        }else {

            $output = array('info' => 'error', 'code' => -202, 'data' => "Insert error");
            print(json_encode($output));
        }

    }

    function rework(){

        $timeStamp = input('post.timeStamp');
        $data = input('post.desc/a');
        $ko = input('post.ko');
        $actionId = input('post.actionId/a');
        $success =  input('post.success');
//        //获取输入的actionId数组
//        $array = explode(",",$actionId);

        $dataBaseConfig =  [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];

        if($ko==""||$data==""||$actionId==""||$timeStamp==""){
            $output = array('info' => 'error', 'code' => -200, 'data' => "Input error");
            exit(json_encode($output));
        }
        //重复检测
        $manager = new Manager("mongodb://localhost:27017");
        $filter  = ['failDesc' => $data , 'ko' => (int)$ko, 'timeStamp' =>(int)$timeStamp,'actionId'=>$actionId];
        $query   = new Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.failureinfo', $query)->toArray();
        if(count($rows) != 0) {
            $output = array('info' => 'error', 'code' => -201, 'data' => 'Repeat Input');
            exit(json_encode($output));
        }

        $db = new mongo\Connection($dataBaseConfig);
        $autoId = getNextIdFromTable('failureInfo');

        $rs = $db->name('troubleshooting.failureinfo')
                 ->insert(["failID" => $autoId,
                        "timeStamp" => (int)$timeStamp,
                         'failDesc' => $data,
                               "ko" => (int)$ko,
                         "actionId" => $actionId,
                           "success"=>(int)$success]);

        if($rs) {

            $output = array('info' => 'correct', 'code' => 200, 'data' => "success");
            print(json_encode($output));
        }else {

            $output = array('info' => 'error', 'code' => -200, 'data' => "Insert error");
            print(json_encode($output));
        }
    }


    function  test (){
        echo (time());

        echo "<br />";
        date_default_timezone_set('Asia/Shanghai');
        echo date('Y-m-d H:i:s');
        echo "<br />";
        date_default_timezone_set('UTC');
        echo date('Y-m-d H:i:s');
        echo "<br />";
        echo (time());
//        echo "<br />";
//        $gmt_date = date('Y-m-d H:i:s',time() - date('Z'));
//        echo ($gmt_date);
        echo "<br />";
        echo gmdate('Y-m-d H:i:s',time() + 8*3600);
    }
}