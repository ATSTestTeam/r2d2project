<?php
namespace  app\R2D2\controller;
use think\mongo;
use  think\Request;
class  Historyoperate {

    //统计历史出错数量和详情
    function historySum () {

        $span = input('span');
        $ko   = input('ko');
        $pn   = input('pn');
        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];
        $db = new mongo\Connection($dataBaseConfig);

        //组合查询的map 添加不确定的查询参数
        $map = array();
        if(!empty($ko)) {
            $map['ko'] = (int)$ko;
        }
        if(!empty($pn)) {
            $map['failDesc.pn'] = $pn;
        }
        if ($span =='d'||$span =='w'||$span =='m'||$span =='y'){

            $rs = $db->name('troubleshooting.failureinfo')
                ->where($map)
                ->whereTime('timeStamp',$span)
                ->select();

            $count = count($rs);
            $output = array('info' => 'success', 'code' => 200, 'data' => $rs,'count' => $count);
            exit(json_encode($output));

        }else {
            $output = array('info' => 'error', 'code' => -200, 'data' => "Please Choose Span");
            exit(json_encode($output));
        }
    }

}
