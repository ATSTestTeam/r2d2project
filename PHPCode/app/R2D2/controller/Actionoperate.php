<?php
namespace  app\R2D2\controller;
use think\mongo;
use  think\Request;

class Actionoperate {

    //增加action字段
    function insert()
    {

        //上传文件类型列表
        $uptypes=array(
            'image/jpg',
            'image/jpeg',
            'image/png',
            'image/pjpeg',
            'image/gif',
            'image/bmp',
            'image/x-png'
        );
        $destination_folder="uploading/"; //上传文件路径


        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];
        $ko = input('post.ko');
        $descEn = input('post.descEn');
        $descCn = input('post.descCn');
        $descGe = input('post.descGe');
        $files = input('file.photo');

        $picArray = array();

        if($files) {
            foreach ($files as $file){

                $info=$file->rule('uniqid')->move($destination_folder);
                $imageURL = 'http://' . $_SERVER['SERVER_NAME'] . '/'.$info->getPath().$info->getBasename();
//                echo $imageURL;
                array_push($picArray,$imageURL);
            }

        }



        if ($ko=="") {
            $output = array('info' => 'error', 'code' => -200, 'data' => 'please input ko.');
            exit(json_encode($output));

        } else if (empty($descEn)) {
            $output = array('info' => 'error', 'code' => -201, 'data' => 'please input English description.');
            exit(json_encode($output));

        } else {
            $autoId = getNextIdFromTable('actionform');
            $db = new mongo\Connection($dataBaseConfig);
            if (!$files) {
                $rs = $db->name('troubleshooting.actionform')->insert(["actionID" => $autoId,
                    'actionDesc' => ['cn' => $descCn, 'en' => $descEn, 'ger' => $descGe,], "ko" => (int)$ko] );
            }else {

                $rs = $db->name('troubleshooting.actionform')->insert(["actionID" => $autoId,
                    'actionDesc' => ['cn' => $descCn, 'en' => $descEn, 'ger' => $descGe,], "ko" => (int)$ko, "pic"=>$picArray ] );

            }



            if ($rs == 1) {
                $output = array('info' => 'correct', 'code' => 200, 'data' => 'insert success.');
            } else {
                $output = array('info' => 'error', 'code' => -202, 'data' => 'insert error.');
            }

            exit(json_encode($output));
        }
    }

    //查询action字段
    function query()
    {
        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];
        $ko = input('get.ko');
        $actionId = input('get.actionId');
        $page = input('get.page');

        $db = new mongo\Connection($dataBaseConfig);


        if (empty($actionId) && $ko=="") {


            $rs = $db->name('troubleshooting.actionform')->select();
            $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
            print(json_encode($output));

        }
        if (!empty($ationid) && $ko=="") {

            $rs = $db->name('troubleshooting.actionform')->where('actionID', (int)$actionId)->select();
            $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
            print(json_encode($output));

        }
        if (empty($ationid) && $ko!="") {


            $rs = $db->name('troubleshooting.actionform')->where('ko', (int)$ko)->select();
            $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
            print(json_encode($output));

        }
        if (!empty($ationid) && $ko!="") {


            $rs = $db->name('troubleshooting.actionform')->where('ko', (int)$ko)
                ->where('actionID', (int)$actionId)
//                ->page(3)
                ->select();
            $output = array('info' => 'correct', 'code' => 200, 'data' => $rs);
            print(json_encode($output));
        }

    }


    //更改action字段
    function update()
    {
        $ko = input('get.ko');
        $actionId = input('get.actionId');
        $descEn = input('get.descEn');
        $descCn = input('get.descCn');
        $descGe = input('get.descGe');
        $dataBaseConfig = [
            'hostname' => '127.0.0.1',
            'database' => 'test',
        ];
        $db = new mongo\Connection($dataBaseConfig);
        if (empty($actionId)) {
            $output = array('info' => 'error', 'code' => -200, 'data' => 'please input  actionID.');
            exit(json_encode($output));
        } else if ($ko!="") {

            $rs = $db->name('troubleshooting.actionform')->where('actionID', (int)$actionId)->update(["ko" => (int)$ko]);
            if ($rs == 1) {

            } else {
                $output = array('info' => 'error', 'code' => -200, 'data' => 'insert error.');
                exit(json_encode($output));
            }

        } else{

            if (!empty($descEn)) {
                $rs = $db->name('troubleshooting.actionform')->where('actionID', (int)$actionId)->update(["actionDesc.en" => $descEn]);
                if ($rs == 1) {

                } else {
                    $output = array('info' => 'error', 'code' => -200, 'data' => 'update error.');
                    exit(json_encode($output));
                }

            }
            if (!empty($descCn)) {
                $rs = $db->name('troubleshooting.actionform')->where('actionID', (int)$actionId)->update(["actionDesc.cn" => $descCn]);
                if ($rs == 1) {

                } else {
                    $output = array('info' => 'error', 'code' => -200, 'data' => 'update error.');
                    exit(json_encode($output));
                }
            }
            if (!empty($descGe)) {
                $rs = $db->name('troubleshooting.actionform')->where('actionID', (int)$actionId)->update(["actionDesc.ger" => $descGe]);
                if ($rs == 1) {

                } else {
                    $output = array('info' => 'error', 'code' => -200, 'data' => 'update error.');
                    exit(json_encode($output));
                }
            }
        }
        $output = array('info' => 'correct', 'code' => 200, 'data' => 'update success.');
        exit(json_encode($output));
    }



}
