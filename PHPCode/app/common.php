<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Manager;
use MongoDB\Driver\Query;
use MongoDB\Driver\WriteConcern;


 $dataBaseConfig = [
    'hostname' => '127.0.0.1',
    'database' => 'test',
];

function getNextIdFromTable ($idName) {

    $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    $filter  = ['name'=> $idName];
    $query   = new MongoDB\Driver\Query($filter);
    $rows    = $manager->executeQuery('troubleshooting.autoIncrease', $query)->toArray();
    $data = json_decode(json_encode($rows[0]),1);

    $bulk = new MongoDB\Driver\BulkWrite;
    $bulk-> update(
        ['name' => $idName],
        ['$set' => ['increaseId' => ((int)$data['increaseId'] + 1)],
        ],
        ['multi' => false, 'upsert' => false]
    );
    $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
    $result = $manager->executeBulkWrite('troubleshooting.autoIncrease', $bulk, $writeConcern);

    return (int)$data['increaseId'] + 1;
}

// 应用公共文件
