
<?php


    $output = array();
    $fid = @$_GET['fid'] ? $_GET['fid'] : '';

    if (empty($fid)) {

        $output = array('data'=>NULL, 'info'=>'不传参就想请求数据？', 'code'=>-201);
        exit(json_encode($output));

    }
    if(!empty($fid)) {

        $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");

        // mysql: SELECT * FROM troubleshooting.failInfo WHERE _id='5a2b9aaf96b4a97efbe31a91';
        $id      = new MongoDB\BSON\ObjectId($fid);
        $filter  = ['_id' => $id];

        $query   = new MongoDB\Driver\Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.failInfo', $query)->toArray();


        $output = array('data'=>$rows[0], 'info'=>'correct', 'code'=>200);
        print(json_encode($output));

    }

