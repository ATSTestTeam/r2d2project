<?php
    include('commonFunc.php');
    error_reporting(0);


    $output = array();

    //$type = $_POST['type'] ? $_POST['type'] : '';
    //$ko = $_POST['ko'] ? $_POST['ko'] : '';
    //$actionId = $_POST['actionId'] ? $_POST['actionId'] : '';
    //$descEn = $_POST['descEn'] ? $_POST['descEn'] : '';
    //$descCn = $_POST['descCn'] ? $_POST['descCn'] : '';
    //$descGe = $_POST['descGe'] ? $_POST['descGe'] : '';
    $ko = @$_GET['ko'] ? $_GET['ko'] : '';
    $actionId = @$_GET['actionId'] ? $_GET['actionId'] : '';

    $descEn = $_GET['descEn'] ? $_GET['descEn'] : '';
    $descCn = @$_GET['descCn'] ? $_GET['descCn'] : '';
    $descGe = @$_GET['descGe'] ? $_GET['descGe'] : '';
    $opType = @$_GET['type'] ? $_GET['type'] : '';

    if (empty($opType)) {
        $output = array('info'=>'error', 'code'=> -200, 'data'=>'please input type.');
        var_dump($actionId);
        exit(json_encode($output));

    }

    $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
    switch ((int)$opType){
        case 1 ://增
            if (empty($ko)) {
                $output = array('info'=>'error', 'code'=> -200, 'data'=>'please input ko.');
                exit(json_encode($output));

            }else if (empty($descEn)) {
                $output = array('info'=>'error', 'code'=> -201, 'data'=>'please input English description.');
                exit(json_encode($output));

            }else {
                $autoId = getNextIdFromTable('actionform');

                $bulk = new MongoDB\Driver\BulkWrite;
                $document = ['_id' => new MongoDB\BSON\ObjectID,
                    'actionID' => (int)$autoId,
                    'actionDesc' => ['cn' => $descCn,
                        'en' => $descEn,
                        'ger' => $descGe],
                    'ko' => (int)$ko];
                $_id= $bulk->insert($document);

                $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
                $result = $manager->executeBulkWrite('troubleshooting.actionform', $bulk, $writeConcern);

                $output = array('info'=>'correct', 'code'=> 200, 'data'=>'success.');
                exit(json_encode($output));
            }
            break;

        case 2:
            $output = array('info'=>'error', 'code'=> -199, 'data'=>'Cant’t delete.');
            exit(json_encode($output));


        case 3: //改
            if (empty($actionId)) {
                $output = array('info'=>'error', 'code'=> -200, 'data'=>'please input actionID.');
                exit(json_encode($output));
            }
            else if (empty($ko)) {
                $output = array('info'=>'error', 'code'=> -201, 'data'=>'please input ko.');
                exit(json_encode($output));

            }else if (empty($descEn)) {
                $output = array('info'=>'error', 'code'=> -202, 'data'=>'please input English description.');
                exit(json_encode($output));

            }else {


                $bulk = new MongoDB\Driver\BulkWrite;
                $bulk->update(
                    ['actionID' => (int)$actionId],
                    ['$set' => ['actionDesc.en' => $descEn,
                                'actionDesc.cn' => $descCn,
                                'actionDesc.ger' => $descGe,
                                'ko' => (int)$ko],
                    ],
                    ['multi' => false, 'upsert' => false]
                );

                $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
                $result = $manager->executeBulkWrite('troubleshooting.actionform', $bulk, $writeConcern);

                $output = array('info' => 'correct', 'code' => 200, 'data' => 'success.');
                exit(json_encode($output));
            }
            break;

        case 4://查
            if (empty($ationid) && empty($ko)) {

                $filter  = [];
                $query   = new MongoDB\Driver\Query($filter);
                $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
                $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows);
                print(json_encode($output));

            }
            if(!empty($ationid) && empty($ko)) {

                $filter  = ['actionID' => (int)$ationid];
                $query   = new MongoDB\Driver\Query($filter);
                $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
                $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows[0]);
                print(json_encode($output));

            }
            if(empty($ationid) && !empty($ko)) {

                $filter  = ['ko' => (int)$ko];
                $query   = new MongoDB\Driver\Query($filter);
                $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
                $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows);
                print(json_encode($output));

            }
            if(!empty($ationid) && !empty($ko)) {

                $filter  = ['actionID' => (int)$ationid, 'ko' => (int)$ko];
                $query   = new MongoDB\Driver\Query($filter);
                $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
                $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows[0]);
                print(json_encode($output));

            }

            break;
    }

