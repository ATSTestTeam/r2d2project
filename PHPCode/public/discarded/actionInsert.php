<?php

    error_reporting(0);

    $output = array();
    $descEn = $_POST['descEn'] ? $_POST['descEn'] : '';
    $descCn = $_POST['descCn'] ? $_POST['descCn'] : '';
    $descGe = $_POST['descGe'] ? $_POST['descGe'] : '';
    $ko = $_POST['ko'] ? $_POST['ko'] : '';


    if (empty($ko)) {
        $output = array('info'=>'error', 'code'=> -200, 'data'=>'please input ko.');
        exit(json_encode($output));

    }else if (empty($descEn)) {
        $output = array('info'=>'error', 'code'=> -201, 'data'=>'please input English description.');
        exit(json_encode($output));

    }else {

        $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");
        $bulk = new MongoDB\Driver\BulkWrite;
        $document = ['_id' => new MongoDB\BSON\ObjectID,
                     'actionID' => 10002,
                     'actionDesc' => ['cn' => $descCn,
                                      'en' => $descEn,
                                     'ger' => $descGe],
                     'ko' => 3];

        $_id= $bulk->insert($document);

        $writeConcern = new MongoDB\Driver\WriteConcern(MongoDB\Driver\WriteConcern::MAJORITY, 1000);
        $result = $manager->executeBulkWrite('troubleshooting.actionform', $bulk, $writeConcern);

        $output = array('info'=>'correct', 'code'=> 200, 'data'=>'success.');
        exit(json_encode($output));
    }
