
<?php

    error_reporting(0);
    $output = array();
    $ationid = @$_GET['actionid'] ? $_GET['actionid'] : '';
    $ko = @$_GET['ko'] ?  $_GET['ko'] : '';

    $manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");


    if (empty($ationid) && empty($ko)) {

        $filter  = [];
        $query   = new MongoDB\Driver\Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
        $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows);
        print(json_encode($output));


    }
    if(!empty($ationid) && empty($ko)) {

        $filter  = ['actionID' => (int)$ationid];
        $query   = new MongoDB\Driver\Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
        $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows[0]);
        print(json_encode($output));

    }
    if(empty($ationid) && !empty($ko)) {

        $filter  = ['ko' => (int)$ko];
        $query   = new MongoDB\Driver\Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
        $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows);
        print(json_encode($output));

    }
    if(!empty($ationid) && !empty($ko)) {

        $filter  = ['actionID' => (int)$ationid, 'ko' => (int)$ko];
        $query   = new MongoDB\Driver\Query($filter);
        $rows    = $manager->executeQuery('troubleshooting.actionform', $query)->toArray();
        $output = array('info'=>'correct', 'code'=>200, 'data'=>$rows[0]);
        print(json_encode($output));

    }

